$(function () {
    let toggleBtn = $('.toggle-btn');
    let body = $('body');

    toggleBtn.on('click', function () {
        body.toggleClass('toggle-nav');
    })
});

// Slider
$(function () {
   $('.slider').slick();
});
